module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: 'https://www.fastmock.site/mock/94dd2c6ec83e998996ec72b34eb2d90f',
        changeOrigin: true
      }
    }
  }
}
