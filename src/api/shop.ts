import { Product, Shop } from '@/types/model'
import { ShopProductsParams } from '@/types/request'
import request from '@/utils/request'

export function hotListApi () {
  return request<Shop[]>({
    url: '/shop/hot-list',
    method: 'get'
  })
}

export function getShopApi (id: string) {
  return request<Shop>({
    url: `/shop/${id}`,
    method: 'get'
  })
}

export function getShopProductsApi (shopId: string, params: ShopProductsParams) {
  return request<Product[]>({
    url: `/shop/${shopId}/products`,
    method: 'get',
    params
  })
}
