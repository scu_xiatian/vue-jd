import { SearchShop } from '@/types/model'
import { SearchParams } from '@/types/request'
import request from '@/utils/request'

export function hotSearchApi () {
  return request<string[]>({
    url: '/search/hot',
    method: 'get'
  })
}

export function searchShopApi (params: SearchParams) {
  return request<SearchShop[]>({
    url: '/search',
    method: 'get',
    params
  })
}
