import { IdData, Order } from '@/types/model'
import { ConfirmOrderParams } from '@/types/request'
import request from '@/utils/request'

export function getOrdersApi () {
  return request<Order[]>({
    url: '/order',
    method: 'get'
  })
}

export function confirmOrderApi (data: ConfirmOrderParams) {
  return request<IdData>({
    url: '/order',
    method: 'post',
    data
  })
}
