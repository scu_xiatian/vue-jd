import { LoginRegisterParams } from '@/types/request'
import request from '@/utils/request'

export function loginApi (data: LoginRegisterParams) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export function registerApi (data: LoginRegisterParams) {
  return request({
    url: '/user/register',
    method: 'post',
    data
  })
}
