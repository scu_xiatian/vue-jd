import { Address } from '@/types/model'
import request from '@/utils/request'

export function getAddressListApi () {
  return request<Address[]>({
    url: '/address',
    method: 'get'
  })
}

export function getAddressApi (id: string) {
  return request<Address>({
    url: `/address/${id}`,
    method: 'get'
  })
}

export function createAddressApi (data: Partial<Address>) {
  return request({
    url: '/address',
    method: 'post',
    data
  })
}

export function updateAddressApi (data: Partial<Address>) {
  return request({
    url: `/address/${data.id}`,
    method: 'put',
    data
  })
}
