import { defineComponent } from 'vue'
const headUrl = require('@/assets/header.jpg')

export default defineComponent({
  name: 'UserInfo',
  setup () {
    /* render 函数 */
    return () => {
      return (
        <div class="info">
          <img src={ headUrl } />
          <div class="info__name">前端程序猿</div>
          <div class="info__star">
            <div class="iconfont icon-star"></div>
            <div class="info__star__number">16</div>
          </div>
          <div class="info__id">ID: 1069643013</div>
          <div class="pocket">
            <div class="pocket__item">
              <div class="pocket__item__title">红包</div>
              <div class="pocket__item__desc">218</div>
            </div>
            <div class="pocket__item">
              <div class="pocket__item__title">优惠券</div>
              <div class="pocket__item__desc">12张</div>
            </div>
            <div class="pocket__item">
              <div class="pocket__item__title">鲜豆</div>
              <div class="pocket__item__desc">88</div>
            </div>
            <div class="pocket__item">
              <div class="pocket__item__title">白条</div>
              <div class="pocket__item__desc">1000</div>
            </div>
          </div>
        </div>
      )
    }
  }
})
