import { defineComponent } from 'vue'
import Docker from '@/components/Docker'
import UserInfo from './UserInfo'
import Features from './Features'

export default defineComponent({
  name: 'PersonCenter',
  setup () {
    /* render 函数 */
    return () => {
      return (
        <>
          <div class="person__center">
            <header>
              <i class="iconfont icon-user-edit"></i>
            </header>
            <div class="container">
              <UserInfo />
              <Features />
            </div>
          </div>
          <Docker currentIndex={ 3 } />
        </>
      )
    }
  }
})
