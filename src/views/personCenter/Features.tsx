import { defineComponent } from 'vue'
import { useRouter } from 'vue-router'

export default defineComponent({
  name: 'Features',
  setup () {
    const router = useRouter()
    /* render 函数 */
    return () => {
      return (
        <div class="features">
          <div class="feature">
            <div class="feature__icon iconfont icon-money"></div>
            <div class="feature__title">我的钱包</div>
            <div class="iconfont icon-back"></div>
          </div>
          <div class="feature" onClick={ () => { router.push({ name: 'AddressManage' }) } }>
            <div class="feature__icon iconfont icon-location"></div>
            <div class="feature__title">我的地址</div>
            <div class="iconfont icon-back"></div>
          </div>
          <div class="feature">
            <div class="feature__icon iconfont icon-user"></div>
            <div class="feature__title">客服与帮助</div>
            <div class="iconfont icon-back"></div>
          </div>
        </div>
      )
    }
  }
})
