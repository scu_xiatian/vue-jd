import { defineComponent } from 'vue'
import { useRouter } from 'vue-router'

export default defineComponent({
  name: 'TopArea',
  setup () {
    const router = useRouter()
    const handleBackClick = () => {
      router.back()
    }
    /* render 函数 */
    return () => {
      return (
        <div class="top">
          <div class="top__bgcor"></div>
          <div class="top__header">
            <div class="top__header__back" onClick={ handleBackClick }>
              <i class="iconfont icon-back"></i>
            </div>
            确认订单
          </div>
          <div class="top__receiver">
            <div class="top__receiver__title">收货地址</div>
            <div class="top__receiver__address">北京理工大学国防科技园2号楼10层</div>
            <div class="top__receiver__info">
              <span class="top__receiver__name">瑶妹（先生）</span>
              <span class="top__receiver__name">18911024266</span>
            </div>
            <div class="iconfont top__receiver__icon">
              <i class="iconfont icon-back"></i>
            </div>
          </div>
        </div>
      )
    }
  }
})
