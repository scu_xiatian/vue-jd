import useCartEffect from '@/hooks/useCartEffect'
import { defineComponent } from 'vue'
import { useRoute } from 'vue-router'

export default defineComponent({
  name: 'ProductList',
  setup () {
    const route = useRoute()
    const shopId = route.params.shopId as string
    const { purchaseCartArray, cartInfo } = useCartEffect(shopId)
    /* render 函数 */
    return () => {
      const productItems = purchaseCartArray.value
        .map(product => (
          <div class="product__item">
            <img class="product__item__img" src={ product.imgUrl } />
            <div class="product__item__detail">
              <h4 class="product__item__title">{ product.name }</h4>
              <p class="product__item__price">
                <span>
                  <span class="product__item__yen">&yen;</span>
                  { product.price } x { product.count }
                </span>
                <span class="product__item__total">
                  <span class="product__item__yen">&yen;</span>
                  { (product.price * product.count).toFixed(2) }
                </span>
              </p>
            </div>
          </div>
        ))
      return (
        <div class="product">
          <div class="product__title">{ cartInfo.value.shopName }</div>
          <div class="product__wrapper">
            <div class="product__list">{ productItems }</div>
          </div>
        </div>
      )
    }
  }
})
