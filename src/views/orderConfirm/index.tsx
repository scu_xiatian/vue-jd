import { defineComponent } from 'vue'
import TopArea from './TopArea'
import Order from './Order'
import CartInfo from '@/components/CartInfo'
import { useRoute } from 'vue-router'

export default defineComponent({
  name: 'OrderConfirm',
  setup () {
    const route = useRoute()
    const shopId = route.params.shopId as string
    /* render 函数 */
    return () => {
      return (
        <div class="order__confirm">
          <TopArea />
          <CartInfo shopId={ shopId } />
          <Order />
        </div>
      )
    }
  }
})
