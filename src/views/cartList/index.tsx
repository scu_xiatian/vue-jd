import { defineComponent } from 'vue'
import Docker from '@/components/Docker'
import { useCommonCartEffect } from '@/hooks/useCartEffect'
import CartInfo from '@/components/CartInfo'
import { RouterLinkSlotProps } from '@/types'

export default defineComponent({
  name: 'CartList',
  setup () {
    const { shopIdList } = useCommonCartEffect()
    /* render 函数 */
    return () => {
      const shopCartList = shopIdList.value.map(shopId => (
        <router-link to={ `/orderConfirm/${shopId}` } custom v-slots={ {
          default: (props: RouterLinkSlotProps) => {
            return (
              <CartInfo shopId={ shopId } onClick={ props.navigate } />
            )
          }
        } } />
      ))
      return (
        <>
          <div class="cart__list">
            <header>我的全部购物车 ({ shopIdList.value.length })</header>
            <div class="wrapper">
              { shopCartList }
            </div>
          </div>
          <Docker currentIndex={ 1 } />
        </>
      )
    }
  }
})
