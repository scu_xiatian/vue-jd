import { defineComponent, reactive, Ref, ref } from 'vue'
import { useRouter } from 'vue-router'
import { imgBaseUrl } from '@/constant/url'
import { createToast } from '@/components/Toast'
import { UserData } from '@/types/model'
import { loginApi, registerApi } from '@/api/auth'

/* 用户登录注册 */
const useLoginEffect = (userData: UserData) => {
  const router = useRouter()
  // 用户登录
  const handleLogin = async () => {
    const res = await loginApi({
      username: userData.username,
      password: userData.password
    })
    if (res.code === 0) {
      localStorage.setItem('isLogin', '1')
      router.push({ name: 'Home' })
    } else {
      createToast('登录失败')
    }
  }

  return {
    userData,
    handleLogin
  }
}

/* 用户注册 */
const useRegisterEffect = (userData: UserData, isLoginRef: Ref<boolean>) => {
  const handleReister = async () => {
    const res = await registerApi({
      username: userData.username,
      password: userData.password
    })
    if (res.code === 0) {
      isLoginRef.value = true
      createToast('注册成功')
    } else {
      createToast('注册失败')
    }
  }

  return {
    handleReister
  }
}

/* 登录注册页面切换 */
const useLoginRegisterSwitch = () => {
  const isLoginRef = ref(true)
  const handleRegisterClick = () => {
    isLoginRef.value = false
  }
  const handleLoginClick = () => {
    isLoginRef.value = true
  }
  return {
    isLoginRef,
    handleRegisterClick,
    handleLoginClick
  }
}

export default defineComponent({
  name: 'Login',
  setup () {
    const { isLoginRef, handleLoginClick, handleRegisterClick } = useLoginRegisterSwitch()
    // 登录/注册 表单数据
    const userData: UserData = reactive({
      username: 'user',
      password: '123',
      ensurement: ''
    })
    const { handleLogin } = useLoginEffect(userData)
    const { handleReister } = useRegisterEffect(userData, isLoginRef)

    /* render 函数 */
    return () => {
      const isLogin = isLoginRef.value
      /* 按钮区域 */
      const buttonArea = isLogin
        ? (
          <>
            <div class="login__button" onClick={ handleLogin }>登录</div>
            <div class="login__link" onClick={ handleRegisterClick }>立即注册</div>
          </>
        )
        : (
          <>
            <div class="login__button" onClick={ handleReister }>注册</div>
            <div class="login__link" onClick={ handleLoginClick }>已有账号去登录</div>
          </>
        )
      return (
        <div class="login">
          <img class="login__img" src={ `${imgBaseUrl}user.png` } />
          <div class="login__input">
            <input type="text" v-model={ userData.username } placeholder="请输入手机号" />
          </div>
          <div class="login__input">
            <input type="password" v-model={ userData.password } placeholder="请输入密码" />
          </div>
          {
            isLogin ? null : (
              <div class="login__input">
                <input type="password" v-model= { userData.ensurement } placeholder="确认密码" />
              </div>
            )
          }
          { buttonArea }
        </div>
      )
    }
  }
})
