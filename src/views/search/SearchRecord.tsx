import { searchStore } from '@/store/modules/search'
import { defineComponent, PropType } from 'vue'

const RecordPropsDefine = {
  title: {
    type: String,
    required: true
  },
  showClear: {
    type: Boolean,
    default: false
  },
  records: {
    type: Array as PropType<string[]>,
    default: () => []
  },
  onSearch: Function
} as const

export default defineComponent({
  name: 'SearchRecord',
  props: RecordPropsDefine,
  emits: ['search'],
  setup (props, { emit }) {
    /* render 函数 */
    return () => {
      const recordItems = props.records.map(record => (
        <div class="record__list__item" onClick={ () => { emit('search', record) } }>{ record }</div>
      ))
      return (
        <div class="record">
          <div class="record__header">
            <div class="record__header__title">{ props.title }</div>
            <div
              v-show={ props.showClear }
              class="record__header__clear"
              onClick={ searchStore.clearRecords }>
              清除搜索历史
            </div>
          </div>
          <div class="record__list">
            { recordItems }
          </div>
        </div>
      )
    }
  }
})
