import { imgBaseUrl } from '@/constant/url'
import { computed, ComputedRef, defineComponent, ref, watch } from 'vue'
import { useRoute } from 'vue-router'
import useCartEffect, { CartInfo } from '@/hooks/useCartEffect'
import { Product } from '@/types/model'
import { appStore } from '@/store/modules/app'
import { RouterLinkSlotProps } from '@/types'

/* 购物车 */
const useCartInnerEffect = (shopId: string) => {
  const { cartArray, cartInfo, changeCartItem, clearShopCart } = useCartEffect(shopId)

  const changItemCheck = (shopId: string, product: Product) => {
    appStore.changItemCheck({
      shopId,
      product
    })
  }

  const setShopCartSet = () => {
    appStore.setShopCartSet(shopId)
  }

  const cartShowArray = computed(() => {
    return cartArray.value.filter(product => product.count > 0)
  })

  return {
    cartInfo,
    cartShowArray,
    changeCartItem,
    changItemCheck,
    clearShopCart,
    setShopCartSet
  }
}

/* 展示隐藏购物车 */
const useToggleCartEffect = (cartInfo: ComputedRef<CartInfo>) => {
  const visible = ref(false)
  const showCart = computed(() => {
    return visible.value && cartInfo.value.total > 0
  })

  const showCartChange = () => {
    cartInfo.value.total === 0
      ? visible.value = false
      : visible.value = !visible.value
  }

  // 展示列表数量小于0时自动隐藏
  watch(() => cartInfo.value.total, (val) => {
    if (val === 0) {
      visible.value = false
    }
  })

  return {
    showCart,
    showCartChange
  }
}

export default defineComponent({
  name: 'Cart',
  props: {
    shopName: {
      type: String,
      required: true
    }
  },
  setup (props) {
    const route = useRoute()
    const shopId = route.params.id as string
    const {
      cartInfo,
      cartShowArray,
      changeCartItem,
      changItemCheck,
      clearShopCart,
      setShopCartSet
    } = useCartInnerEffect(shopId)
    const { showCart, showCartChange } = useToggleCartEffect(cartInfo)
    /* render 函数 */
    return () => {
      // 产品列表
      const productItems = cartShowArray.value.map((product) => (
        <div class="product__item">
          <div class="product__item__checked" onClick={() => changItemCheck(shopId, product)}>
            <i class={ `iconfont icon-${product.check ? 'checked' : 'uncheck'}` }></i>
          </div>
          <img class="product__item__img" src={ product.imgUrl } />
          <div class="product__item__detail">
            <h4 class="product__item__title">{ product.name }</h4>
            <p class="product__item__price">
              <span class="product__item__yen">&yen;</span>{ product.price }
              <span class="product__item__origin">&yen;{ product.oldPrice}</span>
            </p>
          </div>
          <div class="product__number">
            <span
              class="product__number__minus"
              onClick={ () => changeCartItem(props.shopName, product, -1) }>-</span>
            { product.count }
            <span
              class="product__number__plus"
              onClick={ () => changeCartItem(props.shopName, product, 1) }>+</span>
          </div>
        </div>
      ))
      // 购物车中内容
      const cartProduct = (
        <div v-show={ showCart.value } class="product">
          <div class="product__header">
            <div class="product__header__all">
              <span
                class={ `iconfont icon-${cartInfo.value.allCheck ? 'checked' : 'uncheck'}` }
                onClick={ setShopCartSet }></span>
              全选
            </div>
            <div class="product__header__clear">
              <span onClick={ clearShopCart }>清空购物车</span>
            </div>
          </div>
          { productItems }
        </div>
      )
      // 结算链接 Slot
      const linkSlots = {
        default: (props: RouterLinkSlotProps) => {
          return (
            <div v-show={ cartInfo.value.total > 0 } class="check__btn" onClick={ props.navigate }>去结算</div>
          )
        }
      }
      return (
        <>
          <div v-show={ showCart.value } class="mask" onClick={ showCartChange }></div>
          <div class="cart">
            { cartProduct }
            <div class="check">
              <div class="check__icon">
                <img src={`${imgBaseUrl}basket.png`} onClick={ showCartChange } />
                <div class="check__icon__tag">{ cartInfo.value.count }</div>
              </div>
              <div class="check__info">
                总计: <span class="check__info__price">&yen;{ cartInfo.value.price.toFixed(2) }</span>
              </div>
              <router-link to={ `/orderConfirm/${shopId}` } custom v-slots={ linkSlots }></router-link>
            </div>
          </div>
        </>
      )
    }
  }
})
