import { getShopProductsApi } from '@/api/shop'
import { Product } from '@/types/model'
import { defineComponent, reactive, Ref, ref, watchEffect } from 'vue'
import { useRoute } from 'vue-router'
import useCartEffect from '@/hooks/useCartEffect'

const categorys = [
  { name: '全部商品', tab: 'all' },
  { name: '秒杀', tab: 'seckill' },
  { name: '新鲜水果', tab: 'fruits' },
  { name: '休闲食品', tab: 'food' },
  { name: '时令蔬菜', tab: 'vegetable' },
  { name: '肉蛋家禽', tab: 'meat' }
]

interface Data {
  list: Product[]
}

/* 切换Tab */
const useTabEffect = () => {
  const currentTabRef = ref(categorys[0].tab)
  const handleCategoryClick = (tab: string) => {
    currentTabRef.value = tab
  }
  return {
    currentTabRef,
    handleCategoryClick
  }
}

/* 列表内容 */
const userCurrentListEffect = (tabRef: Ref<string>, shopId: string) => {
  const content = reactive<Data>({
    list: []
  })
  const getContentData = async (tab: string) => {
    const res = await getShopProductsApi(shopId, {
      tab
    })
    if (res.code === 0) {
      content.list = res.data
    }
  }
  watchEffect(() => {
    getContentData(tabRef.value)
  })
  return {
    content
  }
}

export default defineComponent({
  name: 'ShopContent',
  props: {
    shopName: {
      type: String,
      required: true
    }
  },
  setup (props) {
    const route = useRoute()
    const shopId = route.params.id as string
    const { currentTabRef, handleCategoryClick } = useTabEffect()
    const { content } = userCurrentListEffect(currentTabRef, shopId)
    const { cartList, changeCartItem } = useCartEffect(shopId)

    /* render 函数 */
    return () => {
      // 类型列表
      const categoryItems = categorys.map((category) => (
        <div
          class={ `category__item ${category.tab === currentTabRef.value ? 'active' : ''}` }
          onClick={ () => handleCategoryClick(category.tab) }>
          { category.name }
        </div>
      ))
      // 产品列表
      const productItems = content.list.map((product) => (
        <div class="product__item">
          <img class="product__item__img" src={ product.imgUrl } />
          <div class="product__item__detail">
            <h4 class="product__item__title">{ product.name }</h4>
            <p class="product__item__sales">月售 { product.sales } 件</p>
            <p class="product__item__price">
              <span class="product__item__yen">&yen;</span>{ product.price }
              <span class="product__item__origin">&yen;{ product.oldPrice}</span>
            </p>
          </div>
          <div class="product__number">
            <span
              class="product__number__minus"
              onClick={ () => changeCartItem(props.shopName, product, -1) }>-</span>
            { cartList[shopId]?.productList?.[product.id]?.count || 0 }
            <span
              class="product__number__plus"
              onClick={ () => changeCartItem(props.shopName, product, 1) }>+</span>
          </div>
        </div>
      ))
      return (
        <div class="content">
          <aside class="category">
            { categoryItems }
          </aside>
          <div class="product">
            { productItems }
          </div>
        </div>
      )
    }
  }
})
