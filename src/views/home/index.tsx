import { defineComponent } from 'vue'
import StaticPart from './StaticPart'
import NearBy from './NearBy'
import Docker from '@/components/Docker'

export default defineComponent({
  name: 'Home',
  setup () {
    return () => {
      return (
        <>
          <div class="home">
            <StaticPart />
            <div class="gap"></div>
            <NearBy />
          </div>
          <Docker />
        </>
      )
    }
  }
})
