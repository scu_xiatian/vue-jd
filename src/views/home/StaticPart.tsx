import { defineComponent } from 'vue'
import { imgBaseUrl } from '@/constant/url'
import { useRouter } from 'vue-router'

const menus = [
  { imgName: '超市', title: '超市便利' },
  { imgName: '菜市场', title: '菜市场' },
  { imgName: '水果店', title: '水果店' },
  { imgName: '鲜花', title: '鲜花绿植' },
  { imgName: '医药健康', title: '医药健康' },
  { imgName: '家居', title: '家居时尚' },
  { imgName: '蛋糕', title: '烘培蛋糕' },
  { imgName: '签到', title: '签到' },
  { imgName: '大牌免运', title: '大牌免运' },
  { imgName: '红包', title: '红包套餐' }
]

export default defineComponent({
  name: 'Static',
  setup () {
    const router = useRouter()
    return () => {
      // 菜单图标区域
      const menuIcons = menus.map(menu => (
        <div class="icons__item">
          <img src={ `${imgBaseUrl}${menu.imgName}.png` } />
          <p>{ menu.title }</p>
        </div>
      ))
      return (
        <>
          <div class="position">
            <span class="iconfont icon-position"></span>
            北京理工大学国防科技园2号楼10层
            <span class="iconfont icon-bell"></span>
          </div>
          <div class="home__search">
            <span class="iconfont icon-search"></span>
            <span class="search__text" onClick={ () => router.push('/search') }>山姆会员商店优惠商品</span>
          </div>
          <div class="banner">
            <img src={ `${imgBaseUrl}banner.jpg` } />
          </div>
          <div class="icons">
            { menuIcons }
          </div>
        </>
      )
    }
  }
})
