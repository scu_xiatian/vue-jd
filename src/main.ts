import { createApp } from 'vue'
import App from './App'
import router from './router'
import store from './store'
import 'normalize.css'
import './style/index.scss'
// import { registerGlobalComponents } from '@/components'

const app = createApp(App)
// registerGlobalComponents(app)
app.use(store).use(router).mount('#app')
