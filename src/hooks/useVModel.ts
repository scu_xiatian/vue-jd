import { ref, watch } from 'vue'

export function useModel (props: Readonly<Record<string, any>>, key: string, emit: Function) {
  const model = ref(props[key])

  watch(model, (val) => {
    emit(`update:${key}`, val)
  })

  return model
}
