import { searchStore } from '@/store/modules/search'
import { ref } from 'vue'
import { useRouter } from 'vue-router'

/* 搜索功能 */
export const useSearchEffect = () => {
  const router = useRouter()
  const searchText = ref('')
  const search = (record: string = searchText.value, changeRouter = true, callback?: Function) => {
    searchStore.updateRecords(record)
    const page = { name: 'SearchResult', query: { search: record } }
    changeRouter
      ? router.push(page)
      : router.replace(page)
    callback && callback()
  }

  return {
    search,
    searchText
  }
}
