import { UserData } from './model'
import { CartProduct } from './store'

export interface ShopProductsParams {
  tab: string
}

export interface ConfirmOrderParams {
  addressId: string
  shopId: string
  shopName: string
  isCanceled: boolean
  products: Pick<CartProduct, 'id' | 'count'>[]
}

export type LoginRegisterParams = Omit<UserData, 'ensurement'>

export interface SearchParams {
  keyword: string
}
