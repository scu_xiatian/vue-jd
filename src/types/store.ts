import { Product } from './model'

export interface CartProduct extends Product {
  count: number
  check: boolean
}

export interface CartShop {
  shopName: string
  productList: {
    [key: string]: CartProduct | undefined
  }
}

export interface Cart {
  [key: string]: CartShop | undefined
}
