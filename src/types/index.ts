import { useLink } from 'vue-router'

export interface MyResponseType<T = any> {
  code: number;
  message: string;
  data: T;
}

export type RouterLinkSlotProps = ReturnType<typeof useLink>
