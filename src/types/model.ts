export interface UserData {
  username: string;
  password: string;
  ensurement?: string;
}

export interface Shop {
  id: string;
  imgUrl: string;
  title: string;
  sales: number;
  expressLimit: number;
  expressPrice: number;
  desc: string;
}

export interface Product {
  id: string;
  imgUrl: string;
  name: string;
  sales: number;
  price: number;
  oldPrice: number;
}

export interface SearchShop extends Shop {
  products: Product[]
}

export interface Address {
  id: string
  name: string
  phone: string
  city: string
  area: string
  number: string
}

export interface OrderProduct {
  orderSales: number
  product: Pick<Product, 'imgUrl' | 'name' | 'price' | 'sales'>
}

export interface Order {
  address: Address
  shopId: string
  shopName: string
  isCanceled: boolean
  products: OrderProduct[]
}

export interface IdData {
  id: string
}
