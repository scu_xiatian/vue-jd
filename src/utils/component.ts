import { ComponentOptionsMixin, ComponentOptionsWithObjectProps, ComponentOptionsWithoutProps, ComponentPropsOptions, ComputedOptions, DefineComponent, defineComponent, EmitsOptions, MethodOptions } from 'vue'

// 基础Props定义
type BasePropsDefine = {
  readonly onClick: FunctionConstructor;
  readonly onKeyup: FunctionConstructor;
}

export function defineTypeComponent<Props = {}, RawBindings = {}, D = {}, C extends ComputedOptions = {}, M extends MethodOptions = {}, Mixin extends ComponentOptionsMixin = ComponentOptionsMixin, Extends extends ComponentOptionsMixin = ComponentOptionsMixin, E extends EmitsOptions = EmitsOptions, EE extends string = string>(options: ComponentOptionsWithoutProps<Props, RawBindings, D, C, M, Mixin, Extends, E, EE>): DefineComponent<BasePropsDefine, RawBindings, D, C, M, Mixin, Extends, E, EE>;
export function defineTypeComponent<PropsOptions extends Readonly<ComponentPropsOptions>, RawBindings, D, C extends ComputedOptions = {}, M extends MethodOptions = {}, Mixin extends ComponentOptionsMixin = ComponentOptionsMixin, Extends extends ComponentOptionsMixin = ComponentOptionsMixin, E extends EmitsOptions = Record<string, any>, EE extends string = string>(options: ComponentOptionsWithObjectProps<PropsOptions, RawBindings, D, C, M, Mixin, Extends, E, EE>): DefineComponent<BasePropsDefine & PropsOptions, RawBindings, D, C, M, Mixin, Extends, E, EE>;
export function defineTypeComponent (options: any) {
  return defineComponent(options)
}
