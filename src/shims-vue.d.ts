/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare module '*.module.scss' {
  const module: { [key: string]: any }
  export default module
}

declare module '*.jpg' {
  const url: string
  export default url
}