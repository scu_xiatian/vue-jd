import { defineComponent, withModifiers } from 'vue'
import classes from './style.module.scss'

const ModalPropsDefine = {
  visible: {
    type: Boolean,
    default: false
  },
  showClose: {
    type: Boolean,
    default: false
  },
  onClose: Function
} as const

const Modal = defineComponent({
  name: 'Modal',
  props: ModalPropsDefine,
  emits: ['update:visible', 'close'],
  setup (props, { emit, slots }) {
    const closeModal = () => {
      emit('update:visible', false)
      emit('close')
    }
    /* render 函数 */
    return () => {
      return (
        <div v-show={ props.visible } class={ classes.modal } onClick={ closeModal }>
          <div class={ classes.content } onClick={ withModifiers(() => {}, ['stop']) }>
            <div class={ classes.wrapper }>
              { slots.default && slots.default() }
              <i v-show={ props.showClose } class={ `iconfont icon-close ${classes.close}` } onClick={ closeModal }></i>
            </div>
          </div>
        </div>
      )
    }
  }
})

export default Modal
