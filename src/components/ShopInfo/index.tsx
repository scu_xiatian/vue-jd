import { PropType } from 'vue'
import { Shop } from '@/types/model'
import classes from './style.module.scss'
import { defineTypeComponent } from '@/utils/component'

const ShopInfoPropsDefine = {
  shop: {
    type: Object as PropType<Shop>,
    required: true
  },
  hideBorder: {
    type: Boolean,
    default: false
  }
} as const

export default defineTypeComponent({
  name: 'ShopInfo',
  props: ShopInfoPropsDefine,
  setup (props) {
    /* render 函数 */
    return () => {
      return (
        <div class={ `${classes.shop__item} ${props.hideBorder ? '' : classes.shop__content__bordered}` }>
          <img src={ props.shop.imgUrl } />
          <div class={ classes.shop__content }>
            <div class={ classes.shop__content__title }>{ props.shop.title }</div>
            <div class={ classes.shop__content__tags }>
              <span>月售{ props.shop.sales }+</span>
              <span>起送￥{ props.shop.expressLimit }</span>
              <span>基础运费￥{ props.shop.expressPrice }</span>
            </div>
            <p class={ classes.shop__content__highlight}>{ props.shop.desc }</p>
          </div>
        </div>
      )
    }
  }
})
