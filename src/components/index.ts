import { App } from 'vue'
import ShopInfo from './ShopInfo'

const componentList = [
  ShopInfo
]

export const registerGlobalComponents = (app: App) => {
  componentList.forEach(component => {
    app.component(component.name, component)
  })
}
