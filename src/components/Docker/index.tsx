import { RouterLinkSlotProps } from '@/types'
import { defineComponent } from 'vue'
import classes from './style.module.scss'

const tabs = [
  { text: '首页', icon: 'home', name: 'Home' },
  { text: '购物车', icon: 'cart', name: 'CartList' },
  { text: '订单', icon: 'order', name: 'OrderList' },
  { text: '我的', icon: 'my', name: 'PersonCenter' }
]

export default defineComponent({
  text: 'Docker',
  props: {
    currentIndex: {
      type: Number,
      default: 0
    }
  },
  setup (props) {
    return () => {
      /*
        底部容器
      */
      // 底部导航栏
      const bottomTabs = tabs.map((tab, index) => {
        const itemClasses = `${classes.docker__item} ${
          props.currentIndex === index ? classes.active : ''
        }`
        return (
          <router-link to={ { name: tab.name } } custom v-slots= {
            {
              default (linkProps: RouterLinkSlotProps) {
                return (
                  <div class={ itemClasses } onClick={ linkProps.navigate }>
                    <div class={ `iconfont icon-${tab.icon}` }></div>
                    <div class={ classes.docker__title }>{ tab.text }</div>
                  </div>
                )
              }
            }
          }>
          </router-link>
        )
      })
      return (
        <div class={ classes.docker }>
          { bottomTabs }
        </div>
      )
    }
  }
})
