import { useModel } from '@/hooks/useVModel'
import { defineTypeComponent } from '@/utils/component'
import { withKeys } from '@vue/runtime-dom'

const SearchInputPropsDefine = {
  modelValue: {
    type: String,
    default: ''
  },
  placeholder: {
    type: String,
    default: ''
  }
} as const

export default defineTypeComponent({
  name: 'SearchInput',
  props: SearchInputPropsDefine,
  emits: ['keyup', 'update:modelValue'],
  setup (props, { emit }) {
    const searchText = useModel(props, 'modelValue', emit)
    /* render 函数 */
    return () => {
      return (
        <div class="search__input">
          <i class="iconfont icon-search"></i>
          <input
            type="text"
            v-model={ searchText.value } onKeyup={ withKeys(() => emit('keyup'), ['enter']) }
            placeholder={ props.placeholder }
          />
        </div>
      )
    }
  }
})
