import { createApp, defineComponent } from 'vue'
import classes from './style.module.scss'

const Toast = defineComponent({
  name: 'Toast',
  props: {
    message: {
      type: String
    }
  },
  setup (props) {
    return () => {
      return (
        <div class={ classes.toast }>{ props.message }</div>
      )
    }
  }
})

export default Toast

export const createToast = (message: string, timeout = 2000) => {
  const toastInstance = createApp(Toast, {
    message
  })
  const mountNode = document.createElement('div')
  document.body.appendChild(mountNode)
  toastInstance.mount(mountNode)
  // 清除消息
  setTimeout(() => {
    toastInstance.unmount()
    document.body.removeChild(mountNode)
  }, timeout)
}
