import useCartEffect from '@/hooks/useCartEffect'
import { defineTypeComponent } from '@/utils/component'

export default defineTypeComponent({
  name: 'CartInfo',
  props: {
    shopId: {
      type: String,
      required: true
    }
  },
  setup (props) {
    const { purchaseCartArray, cartInfo } = useCartEffect(props.shopId)
    /* render 函数 */
    return () => {
      const productItems = purchaseCartArray.value
        .map(product => (
          <div class="product__item">
            <img class="product__item__img" src={ product.imgUrl } />
            <div class="product__item__detail">
              <h4 class="product__item__title">{ product.name }</h4>
              <p class="product__item__price">
                <span>
                  <span class="product__item__yen">&yen;</span>
                  { product.price } x { product.count }
                </span>
                <span class="product__item__total">
                  <span class="product__item__yen">&yen;</span>
                  { (product.price * product.count).toFixed(2) }
                </span>
              </p>
            </div>
          </div>
        ))
      return (
        <div class="product">
          <div class="product__title">{ cartInfo.value.shopName }</div>
          <div class="product__wrapper">
            <div class="product__list">{ productItems }</div>
          </div>
        </div>
      )
    }
  }
})
