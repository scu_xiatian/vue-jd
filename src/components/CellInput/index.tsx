import { useModel } from '@/hooks/useVModel'
import { defineComponent } from 'vue'

const propsDefine = {
  label: {
    type: String,
    default: ''
  },
  modelValue: {
    type: String,
    default: ''
  },
  placeholder: {
    type: String,
    default: ''
  }
} as const

export default defineComponent({
  name: 'CellInput',
  props: propsDefine,
  emits: ['update:modelValue'],
  setup (props, { emit }) {
    const model = useModel(props, 'modelValue', emit)
    /* render 函数 */
    return () => {
      return (
        <div class="cell__input">
          <div class="cell__input__label">{ props.label }:</div>
          <div class="cell__input__input">
            <input
              type="text"
              placeholder={ props.placeholder }
              v-model={ model } />
          </div>
        </div>
      )
    }
  }
})
